import pokeApi from "../api/pokeApi";
import { Pokemon } from "../interfaces/pokemon-full";
export const getPokemonInfo = async (nameOrId: string) => {
  try {
    const { data: pokemon } = await pokeApi.get<Pokemon>(
      `/detail/pokemon?id=${nameOrId}`
    );
    /*   const { data: pokemon } = await pokeApi.get<Pokemon>(`/pokemon/${id}`);
     */
    return {
      id: pokemon.id,
      name: pokemon.name,
      sprites: pokemon.sprites,
    };
  } catch (error) {
    return null;
  }
};

export const getPokemonInfo2 = async (nameOrId: string) => {
  try {
    const { data: pokemon } = await pokeApi.get<Pokemon>(
      `/name/pokemon?name=${nameOrId}`
    );
    /*   const { data: pokemon } = await pokeApi.get<Pokemon>(`/pokemon/${id}`);
     */
    return {
      id: pokemon.id,
      name: pokemon.name,
      sprites: pokemon.sprites,
    };
  } catch (error) {
    return null;
  }
};
