import Head from "next/head";
import { FC } from "react";
import { NavBar } from "../ui";

interface Props {
  title?: string;
}

const origin = typeof window === "undefined" ? "" : window.location.origin;

export const Layout: FC<Props> = ({ children, title }) => {
  return (
    <>
      <Head>
        <title>{title || "PokemonApp"}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta charSet="utf-8" />
        <meta name="author" content="Jeancarlo Willy" />
        <meta
          name="description"
          content={`Información sobre el pokemon ${title}`}
        />
        <meta name="keywords" content={`${title}, pokemon, pokedex`} />

        <meta property="og:title" content={`Información sobre ${title}`} />
        <meta
          property="og:description"
          content={`Èsta es la página sobre ${title}`}
        />
        <meta property="og:image" content={`${origin}/img/banner-3.png`} />
      </Head>
      <NavBar />
      <main
        style={{
          padding: "20px",
        }}
      >
        {children}
      </main>
    </>
  );
};
