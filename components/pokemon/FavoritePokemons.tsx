import React from "react";
import { FavoriteCardPokemon } from ".";
import { Grid } from "@nextui-org/react";
import { FC } from "react";

interface Props {
  pokemons: number[];
}

const FavoritePokemons: FC<Props> = ({ pokemons }) => {
  return (
    <Grid.Container gap={2} direction="row" justify="flex-start">
      {pokemons.map((pokemonId) => (
        <FavoriteCardPokemon key={pokemonId} pokemonId={pokemonId} />
      ))}
    </Grid.Container>
  );
};

export default FavoritePokemons;
