import { Spacer, Text, useTheme, Link } from "@nextui-org/react";
import NextLink from "next/link";
import Image from "next/image";

export const NavBar = () => {
  const { theme } = useTheme();
  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "start",
        padding: "0px 50px",
        backgroundColor: theme?.colors?.gray900.value,
      }}
    >
      <NextLink href="/" passHref>
        <Link>
          <Text color="white" h2>
            P
          </Text>
          <Text color="white" h3>
            okemón
          </Text>
        </Link>
      </NextLink>
      <Image
        src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/152.png"
        width={70}
        height={70}
        alt="Icono de la App"
      />
      <Spacer css={{ flex: 1 }} />

      <NextLink href="/favorites" passHref>
        <Link>
          <Text color="white">Favoritos</Text>
        </Link>
      </NextLink>
    </div>
  );
};
