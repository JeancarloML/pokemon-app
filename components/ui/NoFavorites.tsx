import { Container, Image, Text } from "@nextui-org/react";
import React from "react";

export const NoFavorites = () => {
  return (
    <Container
      css={{
        display: "flex",
        flexDirection: "column",
        height: "calc(100vh - 100px)",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
      }}
    >
      <Text h1>No hay favoritos</Text>
      <Image
        src="https://media.giphy.com/media/3o7btLq1XyvXqQqXqY/giphy.gif"
        width={250}
        height={250}
        alt="pokemons"
        css={{
          opacity: 0.1,
        }}
      />
    </Container>
  );
};
