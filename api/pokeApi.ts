import axios from "axios";

/* const pokeApi = axios.create({
  baseURL: "https://pokeapi.co/api/v2",
}); */
const pokeApi = axios.create({
  baseURL: "https://pokemon-static-red.vercel.app/api",
});

export default pokeApi;
