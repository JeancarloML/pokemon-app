import type { NextPage } from "next";
import { Layout } from "../components/layouts";
import { GetStaticProps } from "next";
import { pokeApi } from "../api/";
import { PokemonListResponse, SmallPokemon } from "../interfaces/pokemon-list";
import { Card, Grid, Image, Row, Text } from "@nextui-org/react";
import { PokemonCard } from "../components/pokemon/";

interface Props {
  pokemons: SmallPokemon[];
}

const HomePage: NextPage<Props> = ({ pokemons }) => {
  return (
    <Layout title="Listado de Pokemon">
      <Grid.Container gap={2} justify="flex-start">
        {pokemons.map((pokemon) => (
          <PokemonCard key={pokemon.id} pokemon={pokemon} />
        ))}
      </Grid.Container>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (ctx) => {
  /*  const {
    data: { results },
  } = await pokeApi.get<PokemonListResponse>("/pokemon?limit=151");
  // https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/2.svg
  const URL_BASE_IMG =
    "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world";
  const pokemons = results.map((pokemon: SmallPokemon) => {
    pokemon.id = pokemon.url.split("/")[6];
    pokemon.imagen = `${URL_BASE_IMG}/${pokemon.id}.svg`;
    return {
      ...pokemon,
    };
  });
  return {
    props: {
      pokemons: [...pokemons],
    },
  }; */

  const { data } = await pokeApi.get<SmallPokemon[]>(
    "/pokemon?offset=251&limit=135"
  );
  return {
    props: {
      pokemons: [...data],
    },
  };
};

export default HomePage;
