# Poke - App

Para instalar las dependencias de la aplicación, ejecutar el siguiente comando:

```bash
yarn install
```

Para ejecutar el proyecto, ejecutar el siguiente comando:

```bash
yarn dev
```
